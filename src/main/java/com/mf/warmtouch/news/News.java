package com.mf.warmtouch.news;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "News")
public class News
{
    @Id
    @GeneratedValue(generator = "inc")
    @GenericGenerator( name = "inc", strategy = "increment")
    private Integer id;
    private String title;
    private String content;
    private Timestamp postdate;

    public News()
    {

    }

    public News(String title, String content, Timestamp postdate)
    {
        this.title = title;
        this.content = content;
        this.postdate = postdate;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public Timestamp getPostdate()
    {
        return postdate;
    }

    public void setPostdate(Timestamp postdate)
    {
        this.postdate = postdate;
    }
}

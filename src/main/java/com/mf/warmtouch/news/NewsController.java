package com.mf.warmtouch.news;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/news")
class NewsController
{
    private NewsService service;

    public NewsController(NewsService service)
    {
        this.service = service;
    }

    @GetMapping("/{id}")
    @ResponseBody
    ResponseEntity<News> findNewsById(@PathVariable Integer id)
    {
        return ResponseEntity.ok(service.findById(id));
    }

//    @GetMapping("/{id}")
//    String findNewsById(@PathVariable Long id)
//    {
//        return "TEST";
//    }
}

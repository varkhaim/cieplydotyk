package com.mf.warmtouch.news;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
class NewsService
{
    private NewsRepository repository;

    @Autowired
    public NewsService(NewsRepository repository)
    {
        this.repository = repository;
    }

    public News findById(Integer id)
    {
        Optional<News> myNews = repository.findById(id);
        if (myNews.isPresent())
            return myNews.get();
        return null;
    }
}

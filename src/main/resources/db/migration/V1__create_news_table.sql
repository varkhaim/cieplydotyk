create TABLE news
(
    ID int unsigned primary key auto_increment,
    title varchar (100) not null,
    content varchar (500) not null,
    postdate datetime
);
import * as React from "react";

class newsBox extends React.Component
{
    state = {
    boxContent: {}
    };

    componentDidMount() {
    fetch(`http://localhost:3000/news/${this.props.match.params.id}`)
         .then(data => data.json())
        .then(parsedJson => this.setState({boxContent: parsedJson}));
}

    render()
    {
        return <h1>{this.state.boxContent.content}</h1>;
    }
}

export default newsBox
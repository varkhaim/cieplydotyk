import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import {
    BrowserRouter as Router,
    Route
} from 'react-router-dom'
import newsBox from './components/newsBox';

class App extends Component {

  state = {};

  render() {
    return (
        <Router>
            <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo"/>
              <Route
                  path="/news/:id"
                  component={newsBox}
              />
          </header>

            </div>
        </Router>
    );
  }
}

export default App;